# Surgery Management API

This is the technical assessment submission for my application to the BackEnd Developer position.

---

The application was developed in Nodejs with a MySql database.

## Installation

Steps to test this application.
+ Download and Install [Node.js](https://nodejs.org/en/).
+ Download and Install [MySql Community Server](https://dev.mysql.com/downloads/mysql/).
+ You can create a free account on [Postman](https://web.postman.co/home) to do queries to the server (If you desire).

After installing **Nodejs**, install the following packages to use in node.

### Mysql
```bash
npm install mysql
```

>There is an sql query file **SQLStartUpQueries.sql** in the **.data** folder that has queries to create a database, tables and add data to the tables for quick use of the application.
>
>One admin user is included in the insert statements but more can be created in app.  
>**Username:**  *admin@speurgroup.com*  
**password:**  *admin*

### dotenv

```bash
npm install dotenv
```

>I used dotenv to establish environment variables used in connecting to MySql.  
You will have to create your own. Create a file called .env and add the following variables with the values from your set up. This file should be stored in the root folder of the application


```bash
USER='Your MySql server user'
PASSWORD='Your Mysql server password'
DATABASE='The MySql database to be used'
DB_PORT='Your port'
HOST='Your host'
```

## Launching the web application
open the command line then set the directory to the application root folder then run the following command to start the server.

```shell
node index.js
```
You can then navigate to the browser of your choice and and navigate to 
**host:port** to bring up the start page. You can also query the pages using postman or alternative.



## Contributing
I appreciate any feedback/edits as I am open to learning and developing.

## License
[GNU General Public License v3.0](https://choosealicense.com/licenses/gpl-3.0/)
